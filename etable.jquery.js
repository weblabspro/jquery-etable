(function($) {

	var States = {
		create: 'create',
		edit: 'edit',
		del: 'delete',
		none: 'none'
	};

	$.fn.etable = function(options) {
		var opts = $.extend({}, $.fn.etable.defaults, options);

		var toggleLoading = function(state) {
			if($('#'+opts.loadingId).length < 1) {
				$('body').append('<div class="loading" id="'+opts.loadingId+'"></div>');
			}

			switch(state) {
				case 'show': $('#'+opts.loadingId).show(); break;
				case 'hide': $('#'+opts.loadingId).hide(); break;
			}
		};

		var getMessageText = function(params) {
			var match = params.url.match(/([^\/]+)$/i) || ['save'];
			var state = match[0];

			if(!opts.isContentTypeJson) {
				switch(state) {
					case 'create' || 'add': state = 'save'; break;
					case 'edit' || 'change': state = 'update'; break;
					case 'remove' || 'del': state = 'delete'; break;
				}
			} else {
				switch(params.method) {
					case 'post': state = 'save'; break;
					case 'put' || 'patch': state = 'update'; break;
					case 'delete': state = 'delete'; break;
				}
			}

			var stateSuccessMessage = state + 'SuccessMessage';
			var message = opts[stateSuccessMessage];
			var messageText = '';

			if(typeof(message) === 'object') {
				$.each(message, function(i,v) {
					if(params.data[i] !== undefined ) {
						messageText = v;
					}
				});
				if(messageText === '') {
					messageText = $.fn.etable.defaults[stateSuccessMessage];
				}
			} else {
				messageText = message;
			}
			return messageText;
		}

		var makeRequest = function(params) {
			var getErrorMessage = function(fieldName, value) {
				if($.isNumeric(fieldName)) {
					return value;
				} else {
					return 'Field '+fieldName+': '+value;
				}
			};

			if('undefined' !== params.url) {
				toggleLoading('show');

				var paramsData = {
					url: params.url,
					type: params.method,
					data: params.data,
					dataType: 'json'
				};

				if(opts.isContentTypeJson) {
					if(typeof paramsData.data.id !== 'undefined') {
						paramsData.url = paramsData.url.replace('\?q=', '/' + paramsData.data.id + '?q=');
					}

					paramsData = $.extend({contentType: "application/json"}, paramsData);
					paramsData.data = JSON.stringify(paramsData.data);
				}

				$.ajax(paramsData).always(function() {
					toggleLoading('hide');
				})
				.done(function(id) {

					noty($.extend({type: 'success', text: getMessageText(params)}, defaultNotySettings));

					if('function' === typeof params.done) {
						params.done(id);
					}

					if('function' === typeof opts.success) {
						opts.success(id, params);
					}
				})
				.fail(function(data) {
					var errors = JSON.parse(data.responseText);

					if('object' === typeof errors) {
						if (errors.type == 'last_admin') {
							noty($.extend({type: 'error', text: errors[0]}, defaultNotySettings));
							return false;
						}
						for(var name in errors) {
							var value = errors[name];
							if('string' === typeof value) {
								noty($.extend({type: 'error', text: getErrorMessage(name, value)}, defaultNotySettings));
							} else {
								for(var i=0;i<value.length;i++) {
									noty($.extend({type: 'error', text: getErrorMessage(name, value[i])}, defaultNotySettings));
								}
							}
						}
					} else {
						noty($.extend({type: 'error', text: opts.saveErrorMessage}, defaultNotySettings));
					}

					if('function' === typeof params.fail) {
						params.fail(errors);
					}
				});
			}
		};

		this.each(function() {
			var $this = $(this),
				$addRow = $this.find(opts.sAddRow),
				$addRowButton = $this.find(opts.sAddRowButton),
				$editRows = $this.find(opts.sEditRow);
			var isClosing = true,
				currentState = States.none;

			var addRow = function(data, id) {
				var $clone = $addRow.clone();
				$clone.find('td').each(function() {
					var $inputSelect = $(this).find('input, select'),
						$select = $(this).find('select'),
						template = $(this).data('template');
					var name = $inputSelect.attr('name'),
						value = data[name],
						html = '';

					if($select.length>0) {
						value = $select.find('option[value="'+value+'"]').text();
					}

					if('undefined' === typeof template) {
						html = '<span>' + value + '</span>';
						$inputSelect.replaceWith(html);
					} else {
						html = template;
						$(this).html(html);
						$(this).removeAttr('data-template');
						$(this).attr('data-value', value);
					}
				});

				var $row;

				if('undefined' !== typeof id) {
					$row = $('<tr data-id="'+id+'">'+$clone.html()+'</tr>');
				} else {
					$row = $('<tr>'+$clone.html()+'</tr>');
				}

				$row.find('input[date-type=datepicker]').each(function() {
					$(this).datepicker(defaultDatePickerSettings);
				});

				// if('undefined' !== typeof id) {
				// 	$row.data('id', id);
				// }

				$addRow.after($row);
				handleRow($row);
			};

			var updateAutocomplites = function($element) {
				if('undefined' === typeof $element) {
					$element = $this.find('input[data-autocomplete]');
				}

				$element.each(function() {
					var name = $(this).data('autocomplete');
                    var autocomplete_opt;

                    if('undefined' !== typeof name) {
                        autocomplete_opt = {
                            source: opts.autocomplete[name]
                        };

                        if(opts.autocompleteNoMessages) {
                            autocomplete_opt['messages'] = {
                                noResults: "",
                                results: function () {
                                    return "";
                                }
                            };
                        }

						$(this).autocomplete(autocomplete_opt);
					}
				});
			};

			var clearInputs = function($element) {
				$element.find('input:not([disabled])').each(function() {
					$(this).val('');
				});
			};

			var getData = function($element) {
				var data = {};
				$element.find('*[name]').each(function() {
					var $this = $(this);
					var name = $this.attr('name'),
						value = $this.val();

					data[name] = value;
				});

				return data;
			};

			var trackFocus = function($element, callbacks) {
				var $input = $element.find('input'),
					$select = $element.find('select');

				var checkFocus = function() {
					if (!isClosing) {
						var isValid = true,
							$invalidInput;

						$input.each(function() {
							var $cur = $(this);

							if (isValid && !$cur[0].checkValidity()) {
								isValid = false;
								$invalidInput = $cur;
							}
						});


						if (isValid) {
							isClosing = true;
							callbacks.focusLost();
						} else {
							var name = $invalidInput.attr('name');

							$invalidInput.focus();
							noty($.extend({type: 'error', text: 'Field ' + name + ' is not valid'}, defaultNotySettings));
						}
					}
				};

				$element.click(function(e) {
					e.stopPropagation();
				});
				$addRowButton.click(function(e) {
					e.stopPropagation();
				});

				$('html').click(function() {
					checkFocus();
				});

				$select.change(function() {
					var $tr = $(this).parents('tr');
					var ps = $(this).parent().index(),
						cnt = $tr.find('td').length;
					var np = ps + 1;

					if(ps == cnt - 1) {
						np = ps - 1;
					}

					if(cnt == 1) {
						checkFocus();
					} else {
						$tr.find('td').eq(np).find('input').focus();
					}
				});

				$input.keyup(function(e) {
					if (e.keyCode == 27) {
						isClosing = true;
						callbacks.close();
					} else if (e.keyCode == 13) {
						checkFocus();
					}
				});
			};

			$addRow.hide();
			$addRowButton.click(function() {
				$addRow.show();
				$addRow.find('input').eq(0).focus();
				$addRowButton.parents('tr').hide();
				$(opts.sTooltip).show();
				isClosing = false;
				currentState = States.none;
			});

			trackFocus($addRow, {
				close: function() {
					//clearInputs($addRow);
					$addRow.hide();
					$addRowButton.parents('tr').show();
					$(opts.sTooltip).hide();
					currentState = States.none;
					isClosing = true;
				},
				focusLost: function() {
					var data = getData($addRow);
					//clearInputs($addRow);

					makeRequest({
						url: opts.addUrl,
						method: opts.addMethod,
						data: data,
						actionType: 'add',
						done: function(id) {
							$addRow.hide();
							$addRowButton.parents('tr').show();
							$(opts.sTooltip).hide();
							currentState = States.none;
							clearInputs($addRow);

							addRow(data, id);
							isClosing = true;
						}
					});

					isClosing = false;
				}
			});

			updateAutocomplites();

			$(opts.sClose).click(function(e) {
				isClosing = true;
				$addRow.hide();
				$addRowButton.parents('tr').show();
				$(opts.sTooltip).hide();
				currentState = States.none;
			});

			var columnsNumber = $editRows.eq(0).find('td').length;
			var activeInputs = [];
			var handleRow = function($row) {
				var deleteRow = function() {
					var data = {
						id: $row.data('id')
					};

					makeRequest({
						url: opts.deleteUrl,
						method: opts.deleteMethod,
						actionType: 'delete',
						data: data,
						done: function(id) {
							$row.remove();
						}
					});
				};

				var handleDeleteRow = function() {
					$row.find(opts.sDeleteButton).click(function() {
						currentState = States.del;

						$.emessage({
							title: opts.modal.title,
							body: opts.modal.body.replace('{col1}', $row.find('td:eq(0)').text()),
							confirm: function() {
								deleteRow();
							},
							close: function() {
								currentState = States.none;
							}
						});
					});
				};

				$row.find('td').each(function(i) {
					var $this = $(this),
						position = i % columnsNumber,
						isOnWork = false,
						beforeEditHtml, value;

					var getValue = function() {
						var value = $this.find('input, select').val();

						if(typeof (value = $this.find('input, select').val()) !== 'undefined') {
							return $.trim(value);
						}

						if('undefined' !== typeof $this.attr('data-value')) {
							value = $this.data('value');
						} else {
							value =  $this.text();
						}

						return $.trim(value);

					};

					var setValue = function(value) {
						if('undefined' !== typeof $this.attr('data-value')) {
							$this.data('value', value);
							$this.html(beforeEditHtml);
						} else {
							var $input = $this.find('input'),
								$select = $this.find('select');

							if($input.length > 0) {
								$input.replaceWith('<span>'+value+'</span>');
							}

							if($select.length > 0) {
								var showValue = $select.find('option[value='+value+']').text();
								if (value !== 3) {
									var adminCount = 0;
									$('.etable').find('tr td[data-role-id]').each(function (elem) {
										if ($(this).attr('data-role-id') == 3) {
											adminCount++;
										}
									});
									if (adminCount == 1 && $select.parent().attr('data-role-id') == 3) {
										showValue = "Admin";
									}
								}
								$select.parent().attr('data-role-id', value);
								$select.replaceWith('<span>'+showValue+'</span>');
							}
						}

						handleDeleteRow();
					};

					var focusOut = function(skip_value) {
						var $input = $this.find('input, select');

						if($input.length < 1) {
							return;
						}

						var name = $input.attr('name'),
							newValue = getValue(),
							oldValue = value;

						if($input[0].checkValidity()) {
							if(value != newValue) {
								data = {};
								data['id'] = $this.parent().data('id');
								data[name] = newValue;

                                if (!skip_value) {
                                    makeRequest({
                                        url: opts.editUrl,
                                        method: opts.editMethod,
                                        actionType: 'edit',
                                        data: data,
                                        fail: function(errors) {
                                            var place = oldValue;

                                            if($input.get(0).tagName === 'SELECT') {
                                                place = $input.find('option[value='+place+']').html();
                                            }

                                            $this.html('<span>'+place+'</span>');
                                        }
                                    });

									value = newValue;
								}
							}
						} else {
							noty($.extend({type: 'error', text: 'Field ' + name + ' is not valid'}, defaultNotySettings));
						}
						
						isOnWork = false;
						currentState = States.none;
						setValue(value);
						
						setTimeout(function() {
							if(activeInputs.length < 1) {
								$(opts.sTooltip).hide();
							}
						}, 20);

						activeInputs = [];
					};

					if (!$this.hasClass('not_edited')) {
						$this.click(function (e) {
							if (isOnWork && $(e.target).closest(opts.sDeleteButton).length) {
								focusOut();
								$row.find(opts.sDeleteButton).click();
								return;
							}

							e.stopPropagation();

							if (isOnWork || currentState != States.none) {
								return;
							}

							for (var i = 0; i < activeInputs.length; i++) {
								activeInputs[i].find('input, select').trigger('blur');
							}

							activeInputs.push($this);
							$(opts.sTooltip).show();

							isOnWork = true;
							value = getValue();
							var $cloneCell = $addRow.find('td').eq(position).clone();
							$cloneCell.find('input, select').val('');

							beforeEditHtml = $this.html();
							$this.html($cloneCell.html());

							$this.find('input[date-type=datepicker]').each(function () {
								$(this).removeClass('hasDatepicker').attr('id', '');
								$(this).datepicker(defaultDatePickerSettings);
							});

							var $inputSelect = $this.find('input, select'),
								$select = $this.find('select');

							if ($select.length > 0) {
								value = $select.find('option:contains("' + value + '")').val();
							}

                            $inputSelect
                                .val(value)
                                .focus();

                            $select.change(function () {
                                focusOut();
                                e.stopPropagation();
                            });

                            $this.keyup(function (e) {
                                if (e.keyCode == 27) {
                                    $inputSelect.val(value);
                                    focusOut(true);
                                    e.stopPropagation();
                                } else if (e.keyCode == 13) {
                                    focusOut();
                                    e.stopPropagation();
                                }
                            });

                            updateAutocomplites($inputSelect);
						});
					}
					 
					$('html').click(function(e) {
						if(isOnWork) {
							focusOut();
						}
					});
				});

				handleDeleteRow();
			};

			$editRows.each(function(i) {
				handleRow($(this));
			});
			//handleRow($editRows);

			$(opts.sClose).click(function() {
				for(var i=0;i<activeInputs.length;i++) {
					activeInputs[i].find('input, select').trigger('blur');
				}
				$(opts.sTooltip).hide();
			});

			$this.find('input[date-type=datepicker]').datepicker(defaultDatePickerSettings);

			$('html').on('click', '#ui-datepicker-div', function(e) {
				e.stopPropagation();
			});

		});
	};

	var defaultNotySettings = {
		maxVisible: 3,
		layout: 'topRight',
		timeout: 5000,
		killer: true
	};

	var defaultDatePickerSettings = {
		changeMonth: true,
		changeYear: true,
		buttonImageOnly: true,
		dateFormat: "dd/mm/y",
		minDate: 0
	};

	$.fn.etable.defaults = {
		sAddRow: '.add-row',
		sEditRow: 'tr[data-id]',
		sAddRowButton: '.add-row-button',
		sTooltip: '.tooltip',
		sClose: '.close',
		sDeleteButton: '.delete-button',
		addMethod: 'post',
		editMethod: 'post',
		deleteMethod: 'post',
		saveSuccessMessage: 'Saved',
		updateSuccessMessage: 'Saved',
		deleteSuccessMessage: 'Saved',
		saveErrorMessage: 'Error during save',
		loadingId: 'etable-loading',
        autocompleteNoMessages: false,
		modal: {
			title: 'Are you sure?',
			body: 'Confirm your action'
		},
		"isContentTypeJson": false
	};

})(jQuery);
